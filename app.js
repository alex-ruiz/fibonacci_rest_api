const debug = require('debug')('template:app.js');
const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const http = require('http');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const { PORT = 4000 } = process.env;
app.set('port', PORT);

const server = http.createServer(app);

require('./src/routes/index')(app);

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

debug(`Server started on port ${PORT}`);

server.listen(PORT);
