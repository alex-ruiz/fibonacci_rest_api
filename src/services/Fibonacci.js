const getFibonacciValueByIndex = async (index) => {
  let n1 = 0, n2 = 1, nTemp, result;

  for (let i = 0; i <= index; i++) {
    
    result = n1;

    nTemp = (n1+n2);
    n1 = n2;
    n2 = nTemp;
  }
  return result;
}

module.exports = {
  getFibonacciValueByIndex,
};
