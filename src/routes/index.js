const debug = require('debug')('template:routes.js');
const cors = require('cors');

const Fibonacci = require('./Fibonacci');

module.exports = (app) => {
  app.use(cors());

  // ROUTES
  app.use('/api/fibonacci', Fibonacci);

  // END

  app.get('/api', (req, res) => {
    debug(`Request: ${req.url}`);
    res.send({ message: 'You have reached TEMPLATE /api/, congrats!' });
  });

  app.get('/', (_, res) => res.status(200).send({ message: 'Server alive!' }));

  app.get('*', (req, res) => {
    debug('Not path found!', req.get('host'), req.url);
    res.status(404).send({ message: `Path not found: ${req.url}` });
  });
};
