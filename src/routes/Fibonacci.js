const express = require('express');

const router = express.Router();

// Controllers
const { getFibonacci } = require('../controllers/Fibonacci');

// Routes of HelloWorld
router.get('/', getFibonacci);

module.exports = router;
