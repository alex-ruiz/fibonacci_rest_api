const { getFibonacciValueByIndex } = require('../services/Fibonacci');

module.exports.getFibonacci = async (req, res) => {
  try {
    const index = (req.query || {}).index;
    if (!index || index < 0) {
    throw new Error('The "index" must by gratter than 0');
    }
    const serviceResponse = await getFibonacciValueByIndex(index);

    return res.status(200).send(`At index ${index} the value is: ${serviceResponse}`);
  } catch (error) {
    return res.status(500).send(error.message);
  }
};
